\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Gerência de Memória II}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today}
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Memória Virtual}

\begin{frame}
	\frametitle{Conceitos}
	\begin{itemize}
		\item Problema do \alert{bloatware}: programas que usam muito mais memória do que deveriam;
		\item É preciso então fornecer ao usuário a \alert{ilusão} de que o computador tem muito mais memória do que realmente tem;
		\item Some os seguintes tamanhos:
		\begin{itemize}
			\item Código-fonte;
			\item Dados;
			\item Pilha de execução.
		\end{itemize}
		\item \alert{Solução}: manter na memória principal apenas a parte que está \alert{em uso};
		\item O resto fica na memória secundária (disco).
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Conceitos (cont.)}
	\begin{itemize}
		\item \alert{Ideia}: desvincular o \alert{endereçamento lógico} do \alert{endereçamento físico};
		\item Cada programa tem seu próprio \alert{espaço de endereçamento}.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.90\textwidth]{../imagens/virtual-memory}
			\label{fig:virtual-memory}
			\caption{Execução de memória virtual\footnote{Fonte: \url{https://i.ytimg.com/vi/qlH4-oHnBb8/maxresdefault.jpg}}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Implementação}
	\begin{itemize}
		\item Generalização dos conceitos de \alert{registrador-limite} e \alert{registrador-base};
		\item Ao invés de realizar realocação separada para os segmentos de texto e dados, mapeia o espaço de endereçamento completo na memória física;
		\item Vantagens \cite{aleteia}:
		\begin{itemize}
			\item O tamanho dos programas não é limitado pela memória do computador;
			\item Mais programas podem executar ao mesmo tempo.
		\end{itemize}
		\item Pode ser implementada de duas maneiras:
		\begin{description}
			\item[Paginação] Divide a memória física e a memória virtual em \alert{blocos} de \alert{mesmo tamanho} chamados \alert{páginas};
			\item[Segmentação] Divide a memória física e a memória virtual em blocos de \alert{tamanhos diferentes} chamados \alert{segmentos}.
		\end{description}
	\end{itemize}
\end{frame}

\subsection{Segmentação}

\begin{frame}
	\frametitle{Conceitos}
	\begin{itemize}
		\item Quando começa a escrever o código o programador pensa o programa como um conjunto de \alert{métodos, procedimentos ou funções};
		\item Pode incluir também várias \alert{estruturas de dados};
		\item Normalmente, ao escrever o código, ele não se preocupa com o o \alert{endereço de memória} que cada uma dessas estruturas vai ocupar;
		\item A \alert{segmentação} se baseia na \alert{visão do programador}: o programa é uma coleção de \alert{segmentos};
		\item Segmentos são \alert{unidades lógicas}. Um compilador da linguagem C pode criar, por exemplo, os seguintes segmentos:
		\begin{itemize}
			\item Código;
			\item Variáveis globais;
			\item A \emph{heap}, de onde a memória é alocada;
			\item As \alert{pilhas} (\emph{stacks}) utilizadas por cada thread;
			\item A biblioteca C padrão (stdlib.h, stdio.h, etc).
		\end{itemize}
		\item Bibliotecas cuja alocação acontece no \alert{momento da compilação} podem ocupar segmentos diferentes na memória.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Segmentação (gráfico)}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.40\textwidth]{../imagens/segmentacao01}
			\label{fig:segmentacao01}
			\caption{Divisão da memória em segmentos \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}


\begin{frame}
	\frametitle{Mapeamento}
	\begin{itemize}
		\item O endereço lógico do segmento constitui uma dupla: $<segment\_number, offset>$
		\item O mapeamento entre endereços lógicos e físicos é controlado pela \alert{tabela de segmentos}.
	\end{itemize}
	\begin{description}
		\item[Tabela de segmentos] Mapeia endereços físicos bi-dimensionais. Cada entrada contém as seguintes informações:
		\begin{description}
			\item[base] endereço físico inicial do segmento na memória;
			\item[limit] tamanho do segmento.
		\end{description}
		\item[Segment-table base register (STBR)] Endereço da tabela de segmentos na memória;
		\item[Segment-table length register (STLR)] Número de segmentos utilizados por um programa.
	\end{description}
	\begin{center}
		O segmento número $s$ é ilegal se $s < STLR$.
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Visão lógica}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.75\textwidth]{../imagens/segmentacao02}
			\label{fig:segmentacao02}
			\caption{Visão lógica da segmentação \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Segmentação no hardware}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.75\textwidth]{../imagens/segmentacao03}
			\label{fig:segmentacao03}
			\caption{Algoritmo de segmentação implementado no hardware \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Segmentação (exemplo)}
	\begin{center}
		A referência ao byte 1222 do segmento 0 resultaria numa \alert{interrupção}, pois o segmento só possui 1000 bytes.
	\end{center}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.5\textwidth]{../imagens/segmentacao04}
			\label{fig:segmentacao04}
			\caption{Exemplo de segmentação \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\subsection{Paginação}

\begin{frame}
	\frametitle{Paginação \cite{aleteia}}
	\begin{itemize}
		\item Técnica de gerência de memória onde o espaço de \alert{endereçamento virtual} e o espaço de \alert{endereçamento real} são divididos em \alert{blocos iguais};
		\item Na paginação é permitido o uso de espaço físico de memória não contíguo;
		\item As páginas no espaço virtual são chamadas \alert{páginas virtuais};
		\item A correspondência na memória física é chamada de \alert{moldura de página} (\alert{frame});
		\item Quando um processo precisa ser executado, suas páginas são carregadas em \alert{frames} disponíveis de memória.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Memory Management Unit -- MMU}
	\begin{itemize}
		\item Componente físico que mapeia, em tempo de execução, endereços lógicos para endereços físicos;
		\item Diferentes métodos para mapear endereços físicos e virtuais:
		\begin{enumerate}
			\item O valor do \alert{registrador de realocação} é adicionado a cada endereço gerado pelo processo e enviado para a memória.
			\begin{itemize}
				\item Intel 80x86 possuía quatro registradores para realocação.
			\end{itemize}
			\item O programa do usuário trabalha somente com endereços virtuais.
			\begin{itemize}
				\item O endereçamento em tempo de execução é feito quando o valor da memória é solicitado;
				\item O endereço lógico é vinculado ao endereço físico.
			\end{itemize}
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Registrador de realocação}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.70\textwidth]{../imagens/relocation-register}
			\label{fig:relocation-register}
			\caption{Realocação dinâmica utilizando registrador de realocação \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Implementação}
	\begin{itemize}
		\item Páginas e molduras têm sempre o \alert{mesmo tamanho};
		\item O tamanho das páginas é uma potência de 2, que normalmente varia entre $2^{12}$ e $2^{22}$  (4.096 - 4.194.304) bytes;
		\item Endereços virtuais utilizam a \alert{MMU} para mapear endereços físicos e virtuais;
		\item O mapeamento é realizado através de \alert{tabelas de páginas};
		\item Cada página virtual possui uma entrada na tabela \alert{ETP -- Entrada na Tabela de Páginas};
		\item A tabela permite ao SO localizar a página correspondente, seja em memória ou no disco;
		\item Ainda possui \alert{fragmentação interna}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Hardware}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/paging-hardware}
			\label{fig:paging-hardware}
			\caption{Exemplo de paginação no hardware \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Acesso à memória}
	\begin{itemize}
		\item Passos no acesso à memória:
		\begin{enumerate}
			\item A CPU envia o endereço virtual à MMU;
			\item Na MMU o endereço virtual é dividido em $(p,d)$, onde $p$ é a página e $d$ é o deslocamento dentro da página;
			\item A MMU utiliza a página $p$ para acessar a tabela de páginas e recuperar o frame $f$ no qual a página $p$ reside;
			\item A MMU substitui o $p$ por $f$ e coloca o endereço $(f, d)$ no barramento.
		\end{enumerate}
		\item O endereço virtual é formado pelo número da página virtual (NPV) e um deslocamento dentro da página;
		\item O NPV \alert{identifica unicamente} uma página virtual: o deslocamento é o \alert{índice};
		\item Calcula-se o endereço físico somando o \alert{endereço do frame} com o \alert{deslocamento} contido no endereço virtual.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Mapeamento (gráfico)}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/paging-maping}
			\label{fig:paging-maping}
			\caption{Algoritmo de mapeamento \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Exemplo}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.65\textwidth]{../imagens/paging-example}
			\label{fig:paging-example}
			\caption{Exemplo de organização em páginas \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Acesso}
	\begin{enumerate}
		\item O programa tenta acessar o endereço 0 (MOVE REG, 0);
		\item O endereço virtual 0 é enviado à MMU;
		\item A MMU conclui que o endereço virtual está na página 0 (endereços 0 a 4095);
		\item O processo de mapeamento está na moldura de página (\emph{frame}) 2 (endereços 8192 a 12287);
		\item A MMU transforma o endereço entregue para 8192 e coloca o valor no barramento.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Acesso (gráfico)}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.65\textwidth]{../imagens/paging-example02}
			\label{fig:paging-example02}
			\caption{Acesso à página \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Outro exemplo}
	\begin{enumerate}
		\item O endereço virtual 20500 está a 20 bytes do início da \alert{página virtual 5} (endereços virtuais de 20480 a 24575);
		\item A página virtual 5 está na \alert{página real 3} (endereços reais de 12288 a 16383);
		\item O endereço é mapeado no endereço real 12288 + 20 = 12308.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Outro exemplo (gráfico)}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.75\textwidth]{../imagens/paging-example03}
			\label{fig:paging-example03}
			\caption{Acesso à página \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Layout do espaço de memória}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.60\textwidth]{../imagens/linuxFlexibleAddressSpaceLayout}
			\label{fig:linuxFlexibleAddressSpaceLayout}
			\caption{Layout do espaço de endereçamento de memória no Linux \cite{duarte}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Comparativo}
	\begin{figure}[ht!]
		\centering
		\includegraphics[width=0.60\textwidth]{../imagens/comparativo}
		\label{fig:comparativo}
		\caption{Comparação entre segmentação e paginação \cite{tanenbaum2012}}
	\end{figure}
\end{frame}

\section{Alocação de páginas}

\begin{frame}
	\frametitle{Considerações sobre paginação}
	\begin{itemize}
		\item O espaço de endereçamento virtual é, geralmente, \alert{bem maior} que a memória física disponível;
		\item Algumas páginas não estão mapeadas na memória;
		\item Cada página virtual do processo possui uma entrada na tabela ETP;
		\item Cada entrada possui um \alert{bit de validade}, que indica se a página está ou não na memória principal;
		\item Bit de validade:
		\begin{itemize}
			\item 1 -> está na memória;
			\item 0 -> não está na memória.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Bit de validade}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.6\textwidth]{../imagens/valid-invalid-bit}
			\label{fig:valid-invalid-bit}
			\caption{Utilização de bit de validade \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Validação}
	\begin{itemize}
		\item Quando o processo faz referência a um endereço virtual o sistema verifica, através do \alert{bit de validade}, se a página está ou não na \alert{memória principal};
		\item Se não estiver é necessário \alert{transferir} a página da memória secundária para a memória principal;
		\item Quando a página é transferida, diz-se que houve um \alert{page fault} (falha de página) no sistema;
		\item A \alert{taxa de paginação} indica muito ou pouco \alert{overhead} de processamento.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Análise}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.9\textwidth]{../imagens/vmstat1}
			\label{fig:vmstat1}
			\caption{Ferramentas de análise da memória virtual}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Análise II}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.9\textwidth]{../imagens/vmstat2}
			\label{fig:vmstat2}
			\caption{Servidor com muita carga}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Gerenciamento de page fault \cite{aleteia}}
	\begin{enumerate}
		\item Verificar se a referência de memória desse processo está dentro do seu espaço de endereçamento;
		\item Se a referência for inválida, o processo é finalizado;
		\item Se a referência for válida, toma-se uma das posições de memória para colocar a ``nova'' página;
		\item É feita uma operação de disco para recuperar a página solicitada;
		\item Quando o disco é lido, procede-se à modificação da tabela de páginas, indicando que a página agora está em memória (bit = 1).
		\item A instrução é reiniciada, agora com a página requerida na memória.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Page fault}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.75\textwidth]{../imagens/page-fault}
			\label{fig:page-fault}
			\caption{Algoritmo para gerenciar falhas de página (\emph{page fault}) \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}


\begin{frame}
	\frametitle{Tabela de páginas -- ETP}
	\begin{itemize}
		\item Guarda informações de mapeamento;
		\item Uma das possibilidades de organização é criar uma tabela para cada processo;
		\item O crescimento da tabela pode ser \alert{grande demais}, considerando o aumento do espaço de endereçamento (64 bits);
		\item Campos comuns à maior parte das tabelas de página:
		\begin{description}
			\item[bit de cache] Se a página pode ser colocada em cache;
			\item[bit de referência] Se a página foi referenciada;
			\item[bit de proteção] Permissão da página para o processo;
			\item[bit de presente/ausente] Página presente/ausente em memória física;
			\item[número do frame] \emph{Frame} onde a página está mapeada
		\end{description}
	\end{itemize}
\end{frame}

\subsection{Tabelas de páginas}

\begin{frame}
	\frametitle{Localização da ETP}
	\begin{itemize}
		\item A tabela de páginas pode ser localizada tanto na MMU quanto na memória principal;
		\item Tabela de páginas \alert{totalmente armazenada na MMU};
		\item \alert{Estratégias híbridas}: MMU e memória principal;
		\item Alguns fatores considerados na escolha da estratégia
		\begin{itemize}
			\item \emph{Overhead} da troca de contexto;
			\item Tamanho da tabela de páginas;
			\item Simplicidade de implementação;
			\item Quantidade de acessos à tabela
			\item Custo.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Localização na MMU}
	\begin{itemize}
		\item A tabela de páginas é mantida na \alert{memória principal};
		\item Componentes de hardware auxiliam no controle da tabela de páginas \cite{galvin2013operating}:
		\begin{description}
			\item[Page-table base register -- PTBR] Aponta para a tabela de páginas;
			\item[Page-table length register -- PTLR] Tamanho da tabela de páginas
		\end{description}
		\item Cada instrução necessita de \alert{dois acessos} à memória:
		\begin{enumerate}
			\item Acessa a ETP;
			\item Acessa o dado/instrução.
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Considerações sobre MMU}
	\begin{itemize}
		\item Utilização de \alert{registradores especiais};
		\item Cada registrador contém uma entrada da tabela;
		\item A tabela de páginas é carregada na \alert{troca de contexto} direto dos registradores;
		\item Vantagens \cite{aleteia}:
		\begin{itemize}
			\item Fácil de implementar;
			\item Mapeamento eficiente;
		\end{itemize}
		\item Desvantagens:
		\begin{itemize}
			\item Inviável para páginas grandes;
			\item Aumento do \emph{overhead} na troca de contexto.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Abordagem híbrida}
	\begin{itemize}
		\item Tenta reduzir o custo de realizar \alert{dois acessos} utilizando um tipo especial de cache chamado TLB -- \emph{Translation Look-aside Buffer};
		\item A tabela de páginas é armazenada na memória principal e um registrador contém o endereço de início;
		\item Quando um endereço virtual é apresentado à MMU primeiro acessa o TLB;
		\item Caso não esteja no TLB, realiza o acesso à memória para procurar o endereço;
		\item Insere a entrada no TLB para as próximas referências;
		\item Vantagens:
		\begin{itemize}
			\item Custo relativamente baixo;
		\end{itemize}
		\item Desvantagens:
		\begin{itemize}
			\item Alguns acessos à memória precisarão pagar o custo adiciona de acesso.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Implementação TLB}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.75\textwidth]{../imagens/tlb}
			\label{fig:tlb}
			\caption{Paginação em hardware utilizando TLB \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\subsection{Políticas de alocação}

\begin{frame}
	\frametitle{Número de frames}
	\begin{itemize}
		\item Há duas alternativas para determinar o \alert{número de frames} que cada processo pode manter na memória principal:
	\end{itemize}
	\begin{description}
		\item[Fixo] Todo processo aloca uma \alert{quantidade fixa} de \emph{frames}.
		\begin{itemize}
			\item Cada processo tem um número máximo de \emph{frames};
			\item Quantos \emph{frames} alocar para cada processo?
		\end{itemize}
		\item[Variável] Número de \emph{frames} depende da \alert{taxa de paginação}.
		\begin{itemize}
			\item Varia de acordo com a ocupação da memória;
			\item Como definir o número máximo por processo?
		\end{itemize}
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Política de alocação}
	\begin{itemize}
		\item Como deve ser alocada a memória livre?
		\item Estratégias:
		\begin{itemize}
			\item \alert{Número mínimo de quadros};
			\item \alert{Alocação igual}. Divide $m$ quadros por $n$ processos. Assim, cada processo tem direito a $\frac{m}{n}$ quadros;
			\item \alert{Alocação proporcional}, levando em consideração o tamanho do processo.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Escopo}
	\begin{itemize}
		\item Escopo da política de alocação de páginas:
		\begin{description}
			\item[Local] Só podem ser selecionados \emph{frames} alocados ao processo.
			\begin{itemize}
				\item Número total de quadros do processo permanece constante;
			\end{itemize}
			\item[Global] Os \emph{frames} são compartilhados entre todos os processos.
			\begin{itemize}
				\item Todas as páginas são candidatas à realocação;
				\item Um processo pode realocar \emph{frames} de outro;
				\item Como gerenciar?
				\item É possível reduzir o espaço de memória de outro processo.
			\end{itemize}
		\end{description}
	\end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../so}
\bibliographystyle{apalike}

\end{document}
