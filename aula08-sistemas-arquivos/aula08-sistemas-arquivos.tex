\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Implementação de sistema de arquivos}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today}
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}
	\frametitle{Definições}
	\begin{description}
		\item[Arquivos] Unidades lógicas de informação criadas por processos. (...) Também são uma espécie de espaço de endereçamento. \cite{tanenbaum1995sistemas}
		\item[Memória secundária] Espaço de endereçamento em arquivos.
		\item[Sistemas de arquivos] Unidade do sistema operacional que trata de:
		\begin{itemize}
			\item Estruturar;
			\item Nomear;
			\item Acessar;
			\item Proteger;
			\item Implementar os arquivos.
		\end{itemize}
		\item[Persistência] O armazenamento dos arquivos não pode ser afetado pelo término do processo.
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Mecanismo do disco}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.7\textwidth]{../imagens/cabeca-disco}
			\label{fig:cabeca-disco}
			\caption{Movimento da cabeça do disco \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Gerência de arquivos}
	\begin{itemize}
		\item O sistema operacional é responsável por fazer a \alert{gerência de arquivos};
		\item Para conseguir realizar suas tarefas é necessário interagir com o \alert{mecanismo do hardware};
		\item \alert{Abstração}: ao solicitar um arquivo, você não está preocupado onde ele está armazenado dentro do disco;
		\item \alert{Organização} dos arquivos no sistema operacional: facilidade/dificuldade de acesso.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Gerenciador de arquivos}
	\begin{itemize}
		\item Quando salvamos o arquivo no disco, estamos executando as seguintes tarefas: 
		\begin{enumerate}
			\item O programa pede ao sistema operacional para escrever o conteúdo de uma arquivo;
			\item O sistema operacional repassa a tarefa para o gerenciador de arquivos (file manager), que é um subconjunto do SO;
			\item O gerenciador de arquivos busca em uma tabela informações sobre o arquivo;
			\item O gerenciador de arquivos busca em uma tabela a localização física do setor que deve conter o byte (cilindro, trilha, setor);
			\item O gerenciador de arquivos instrui o processador de I/O (que libera a CPU de cuidar do processo de transferência) sobre a posição do byte na RAM, e onde ele deve ser colocado no disco;
			\item O processador de I/O formata o dado apropriadamente, e decide o melhor momento de escrevê-lo no disco.
		\end{enumerate}
	\end{itemize}
\end{frame}

\section{Implementação}

\begin{frame}
	\frametitle{Visões}
	\begin{itemize}
		\item O arquivo pode possuir diferentes visões \cite{tanenbaum1995sistemas}:
		\begin{itemize}
			\item Visão do usuário:
			\begin{itemize}
				\item Não há preocupação com o método de armazenamento das informações;
				\item O funcionamento dos discos é irrelevante;
				\item \alert{Identificar} o arquivo é fundamental. Ex.: nome do arquivo.
				\item Perenidade do arquivo.
			\end{itemize}
			\item Visão do sistema operacional:
			\begin{itemize}
				\item \alert{Organização e armazenamento} do arquivo e suas estruturas. Ex.: diretórios e marcadores;
				\item Gerenciamento do espaço em disco;
				\item Confiabilidade e funcionamento.
			\end{itemize}
		\end{itemize}
		\item Ambas as visões precisam ser abordadas pelo Sistema Operacional.
	\end{itemize}
\end{frame}

\subsection{Visão do usuário}

\begin{frame}
	\frametitle{Nomeação}
	\begin{itemize}
		\item Principal regra de identificação para o usuário: \alert{nome do arquivo};
		\item O nome pode utilizar letras maiúsculas e minúsculas;
		\item \alert{Extensão}: identificação do tipo de arquivo;
		\begin{itemize}
			\item Unix/Linux: a extensão do arquivo não importa tanto assim. Saída do comando \texttt{file};
			\item Windows: extensão mapeia o \alert{tipo} do arquivo.
		\end{itemize}
		\item O mapeamento de tipo associa o arquivo a um programa;
		\item O nome do arquivo é uma estrutura lógica.
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Arquivos regulares}
	\begin{itemize}
		\item \alert{Definição}: sequência de bits contendo unidades lógicas criadas pelos usuários;
		\item As sequências de bits precisam ser \alert{decodificadas} em palavras visíveis ao usuário.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.4\textwidth]{../imagens/unicode-ascii}
			\label{fig:unicode-ascii}
			\caption{ASCII e Unicode \footnote{http://paginaspersonales.deusto.es/abaitua/konzeptu/unicode/uc20ch1.html}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Arquivos binários}
	\begin{itemize}
		\item A estrutura dos bits faz referência ao programa que as utiliza;
		\item Importância do conceito de \alert{extensão};
		\item Um arquivo binário executável do Unix possui cinco partes \cite{aleteia}:
		\begin{description}
			\item[Cabeçalho] Identifica o arquivo e seus componentes. Ex.: tamanho das partes, endereço inicial de execução, etc.
			\item[Texto] Pode possuir um conteúdo de texto \alert{codificado};
			\item[Dados] Dados do programa relacionado ao sistema operacional;
			\item[Bits de relocação] Carregamento e relocação de texto nos dados;
			\item[Tabela de símbolos] Sequência de bits que só faz sentido para o programa que a executa;
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Métodos de acesso}
	\begin{description}
		\item[Sequencial] Acesso sequencial
		\begin{itemize}
			\item Informações são processadas em ordem;
			\item Método mais utilizado. Ex.: compiladores e editores.
			\item Operações de \alert{leitura} e \alert{escrita};
			\item A leitura é feita avançando o ponteiro de arquivo para o próximo registro;
			\item A escrita é feita adicionando o novo registro ao final do arquivo;
		\end{itemize}
	\end{description}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.7\textwidth]{../imagens/sequential}
			\label{fig:sequential}
			\caption{Acesso sequencial \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\begin{description}
		\item[Direto] Acesso direto
		\begin{itemize}
			\item Modelo de discos: o arquivo é visto como uma sequência de \alert{blocos} ou \alert{registros};
			\item Não há uma \alert{ordem} pré-determinada para a leitura;
			\item Todo arquivo pode ser acessado a partir do \alert{endereço}.
		\end{itemize}
		\item[Indexado] Tabela de índices de arquivo.
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Exemplos de acesso}
	\lstinputlisting[language=Bash, label=cod:sequential, caption={Acesso sequencial}]{../codigos/sequential.sh}
	\lstinputlisting[language=Bash, label=cod:direct, caption={Acesso direto}]{../codigos/direct.sh}
	\begin{itemize}
		\item n = \alert{número relativo do bloco}
		\item Acesso direto: o arquivo tem \alert{registros lógicos} de tamanho fixo.
		\item O número relativo do bloco (n) permite ao SO decidir onde armazenar o arquivo.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Métodos de acesso}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/access}
			\label{fig:access}
			\caption{Métodos de acesso \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Acesso pelo índice}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/index}
			\label{fig:access}
			\caption{Acesso através do índice \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Operações}
	\begin{itemize}
		\item Algumas operações básicas para serem realizadas nos arquivos:
		\begin{description}
			\item[Criação] Dois procedimentos:
			\begin{enumerate}
				\item Encontrar espaço suficiente;
				\item Identificar o \alert{diretório} onde será armazenado.
			\end{enumerate}
			\item[Escrita] na posição do \alert{ponteiro de escrita};
			\item[Leitura] na posição do \alert{ponteiro de leitura};
			\item[Pesquisa] Reposicionamento do ponteiro de \alert{seek} na posição atual do arquivo;
			\item[Abrir] Coloca na memória principal os principais atributos e a lista de endereços;
			\item[Fechar] Libera o espaço de memória alocado.
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Arquivos abertos}
	\begin{itemize}
		\item É muito importante manter a gestão sobre todos os arquivos que estão abertos;
		\item Várias estruturas de dados diferentes são necessárias para manter o controle:
		\begin{itemize}
			\item \alert{Tabela de arquivos abertos}: controla todos os arquivos que estão abertos;
			\item Ponteiro do arquivo: ponteiro que aponta para a última posição de leitura/escrita, em cada processo, que mantém um arquivo aberto;
			\item \alert{Contador de arquivos abertos}: mantém um contador de todas as vezes que o arquivo é aberto para permitir a remoção da tabela de arquivos abertos quando ele for fechado;
			\item Localização do arquivo no disco
			\item Permissões de acesso: informação armazenada por processo.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Sistema de travas}
	\begin{itemize}
		\item Em alguns sistemas operacionais e sistemas de arquivos um sistema de travas (\emph{locks}) é utilizado para controlar o acesso do sistema operacional;
		\begin{description}
			\item[Shared lock] Similar à trava de leitura: vários arquivos podem obter ao mesmo tempo
			\item[Exclusive lock] Similar à trava de escrita
		\end{description}
		\item Organiza o acesso aos arquivos.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Diretórios}
	\begin{itemize}
		\item \alert{Definição} \cite{aleteia}: tabela de símbolos que traduz os nomes de arquivos a seus agrupamentos (diretórios);
		\item Arquivos que mantém a estrutura do sistema de arquivos;
		\item Entradas associadas aos arquivos;
		\item Ferramenta para \alert{organização} dos arquivos;
		\item \alert{Partições}: suportam estrutura de arquivos e diretórios;
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Particionamento}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/partition}
			\label{fig:partition}
			\caption{Particionamento e sistema de arquivos \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Sistemas de arquivos}
	\begin{itemize}
		\item Os sistemas de arquivos que vemos normalmente são de uso geral;
		\item Alguns sistemas de arquivos são de propósito geral e especial;
		\item Exemplos (Solaris):
		\begin{description}
			\item[tmpfs] Sistemas de arquivos volátil para acesso rápido e temporário;
			\item[objfs] Símbolos do kernel;
			\item[ctfs] Gerenciamento de \emph{daemons};
			\item[lofs] Interface de loopback;
			\item[procfs] Parâmetros do kernel;
			\item[ufs, zfs] Sistemas de arquivos de uso geral.
		\end{description}
	\end{itemize}
\end{frame}

\subsection{Visão do Sistema Operacional}

\begin{frame}
	\frametitle{Atributos}
	\begin{itemize}
		\item Organizamos os arquivos no sistema operacional utilizando um conjunto de atributos:
		\begin{description}
			\item[Nome] Única informação legível disponibilizada para os usuários.
			\item[Identificador] Número de identificação (\emph{tag}) que identifica o arquivo no sistema de arquivos;
			\item[Tipo] Diferentes tipos de arquivo para diferentes finalizados. Ex.: música, vídeo, etc.
			\item[Localização] Ponteiro para o endereço do arquivo no dispositivo;
			\item[Tamanho] Tamanho ocupado;
			\item[Proteção] Controle de permissão e outros controles mais apurados. Ex.: SELinux;
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Estrutura do sistema de arquivos}
	\begin{itemize}
		\item Estrutura dos arquivos:
		\begin{itemize}
			\item Unidade de armazenamento lógico;
			\item Coleção de informações relacionadas.
		\end{itemize}
		\item O sistema de arquivos está na memória secundária (discos);
		\begin{itemize}
			\item A interface com o usuário realiza o mapeamento entre os endereços lógicos e físicos;
			\item Meio de armazenar e recuperar a informação.
		\end{itemize}
		\item Os sistemas de arquivos são organizados em \alert{camadas}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Camadas}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.3\textwidth]{../imagens/fs-layers}
			\label{fig:fs-layers}
			\caption{Camadas do sistema de arquivos \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Organização em camadas}
	\begin{itemize}
		\item A \alert{controladora do disco} coordena as ações entre o os dispositivos e a camada de I/O;
		\item O \alert{sistema de arquivos} recebe o comando de recuperar o arquivo e manda para o dispositivo;
		\item Também realiza a gerência de \emph{buffers} e \emph{cache};
		\begin{description}
			\item[Buffer] Dados ``em trânsito'' para o disco;
			\item[Cache] Dados muito utilizados.
		\end{description}
		\item Módulo de \alert{organização de arquivos} compreende os arquivos e seus endereços lógicos e físicos;
		\item o sistema de arquivos \alert{lógico} controla os metadados;
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Implementação}
	\begin{itemize}
		\item Como são implementadas as chamadas de sistema (SYSCALL)?
		\begin{description}
			\item[Bloco de controle de inicialização] Informações necessárias para iniciar o sistema operacional a partir daquele volume;
			\item[Bloco de controle de volumes] Contém os detalhes do volume:
			\begin{itemize}
				\item Número total de blocos, número de livres, tamanho do bloco, etc;
				\item Estrutura de \alert{diretórios} organiza os arquivos (inodes)
			\end{itemize}
			\item[Bloco de controle do arquivo (FCB)] Presente em cada arquivo, contém os detalhes sobre o arquivo
			\begin{itemize}
				\item EXT3, EXT4 armazenam número do inode, permissões, etc;
				\item NTFS armazena os dados em uma tabela de arquivos no formato relacional.
			\end{itemize}
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Layout do sistema de arquivos}
    \begin{figure}
		\includegraphics[width=0.9\textwidth]{../imagens/fs-layout}
		\label{fig:fs-layout}
		\caption{Um possível layout de sistema de arquivos \cite{tanenbaum1995sistemas}}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Estruturas na memória}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.7\textwidth]{../imagens/open-read-file}
			\label{fig:open-read-file}
			\caption{Abrir (a) e ler (b) um arquivo \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\section{Alocação}

\begin{frame}
	\frametitle{Alocação contígua}
	\begin{itemize}
		\item Um método de alocação define como os blocos de dados são alocados no disco.
		\item \alert{Método de alocação contígua}: cada arquivo ocupa um conjunto contíguo de blocos.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/disk-maping}
			\label{fig:disk-maping}
			\caption{Mapeamento lógico para físico \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Listas encadeadas}
	\begin{itemize}
		\item Cada arquivo é um conjunto de blocos;
		\item O arquivo acaba no ponteiro nulo;
		\item Mesmo algoritmo utilizado em gerência de memória.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/lista-encadeada02}
			\label{fig:lista-encadeada02}
			\caption{Exemplo de lista encadeada \cite{aleteia}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{FAT -- File allocation table}
	\begin{itemize}
		\item Há uma tabela no começo do volume identificada pelo número;
		\item Parecida com a lista encadeada, mais eficiente no disco.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.9\textwidth]{../imagens/fs-vfat}
			\label{fig:fs-vfat}
			\caption{Um arquivo no formato FAT \cite{tanenbaum1995sistemas}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{FAT -- Exemplo}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.55\textwidth]{../imagens/fat}
			\label{fig:fat}
			\caption{Exemplo de implementação do FAT \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Índices (inodes)}
	\begin{itemize}
		\item Cada arquivo tem um \alert{ponteiro de índice} que aponta para seus blocos de dados.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.7\textwidth]{../imagens/inodes}
			\label{fig:inodes}
			\caption{Exemplo de alocação em inodes \cite{hirata}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
    \frametitle{Journalling file systems}
    \begin{itemize}
        \item Considere a operação de remover um arquivo em sistemas Unix:
        \begin{enumerate}
            \item Remover o arquivo do seu diretório;
            \item Liberar o i-node do conjunto de i-nodes livres;
            \item Retornar todos os blocos ao conjunto de blocos livres.
        \end{enumerate}
        \item Se não houver uma falha, a ordem em que os passos forem realizados não importa;
        \item O sistema baseado em \emph{journalling} primeiro escreve um registro das três ações que precisam ser realizadas no disco;
        \item As operações só acontecem após o registro salvo no disco;
        \item Se houver qualquer falha, o registro pode ser consultado para verificar se há operações pendentes e realizá-las;
        \item As operações registradas devem ser \alert{idempotentes} e \alert{atômicas};
        \item Ex.: NTFS, EXT2, EXT3
    \end{itemize}
\end{frame}





\begin{frame}
	\frametitle{Performance}
	\begin{itemize}
		\item O melhor método depende do tipo de acesso ao arquivo;
		\item O método contíguo é ótima para acesso sequencial e aleatório;
		\item Listas encadeadas são ótimas para acesso sequencial;
		\item Declara o tipo de acesso na criação;
		\item A utilização de índices é mais complexa:
		\begin{itemize}
			\item Um único acesso ao bloco pode precisar de duas leituras de índice mais uma leitura do bloco;
			\item A utilização de \emph{clustering} pode aumentar o throughput e diminir o consumo de CPU.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{IOPS}
	\begin{itemize}
		\item Adicionar instruções no fluxo de execução que ``economizam'' uma operação de disco parece razoável;
		\item Intel Core i7 Extreme Edition 990x (2011) at 3.46Ghz = 159,000 MIPS \footnote{Fonte: \url{http://en.wikipedia.org/wiki/Instructions_per_second}}
		\item Typical disk drive at 250 I/Os per second; 159,000 MIPS / 250 = 630 million instructions during one disk I/O;
		\item Fast SSD drives provide 60,000 IOPS; 159,000 MIPS / 60,000 = 2.65 millions instructions during one disk I/O
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../so}
\bibliographystyle{apalike}

\end{document}