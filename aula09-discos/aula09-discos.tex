\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Sistemas de arquivos -- Discos}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today}
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}
	\frametitle{Definições}
	\begin{description}
		\item[Arquivos] Unidades lógicas de informação criadas por processos. (...) Também são uma espécie de espaço de endereçamento. \cite{tanenbaum1995sistemas}
		\item[Memória secundária] Espaço de endereçamento em arquivos.
		\item[Sistemas de arquivos] Unidade do sistema operacional que trata de:
		\begin{itemize}
			\item Estruturar;
			\item Nomear;
			\item Acessar;
			\item Proteger;
			\item Implementar os arquivos.
		\end{itemize}
		\item[Persistência] O armazenamento dos arquivos não pode ser afetado pelo término do processo.
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Mecanismo do disco}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.7\textwidth]{../imagens/cabeca-disco}
			\label{fig:cabeca-disco}
			\caption{Movimento da cabeça do disco \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Gerência de arquivos}
	\begin{itemize}
		\item O sistema operacional é responsável por fazer a \alert{gerência de arquivos};
		\item Para conseguir realizar suas tarefas é necessário interagir com o \alert{mecanismo do hardware};
		\item \alert{Abstração}: ao solicitar um arquivo, você não está preocupado onde ele está armazenado dentro do disco;
		\item \alert{Organização} dos arquivos no sistema operacional: facilidade/dificuldade de acesso.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Gerenciador de arquivos}
	\begin{itemize}
		\item Quando salvamos o arquivo no disco, estamos executando as seguintes tarefas: 
		\begin{enumerate}
			\item O programa pede ao sistema operacional para escrever o conteúdo de uma arquivo;
			\item O sistema operacional repassa a tarefa para o gerenciador de arquivos (file manager), que é um subconjunto do SO;
			\item O gerenciador de arquivos busca em uma tabela informações sobre o arquivo;
			\item O gerenciador de arquivos busca em uma tabela a localização física do setor que deve conter o byte (cilindro, trilha, setor);
			\item O gerenciador de arquivos instrui o processador de I/O (que libera a CPU de cuidar do processo de transferência) sobre a posição do byte na RAM, e onde ele deve ser colocado no disco;
			\item O processador de I/O formata o dado apropriadamente, e decide o melhor momento de escrevê-lo no disco.
		\end{enumerate}
	\end{itemize}
\end{frame}

\section{Mecanismos de armazenamento}

\begin{frame}
	\frametitle{Discos magnéticos \cite{galvin2013operating}}
	\begin{itemize}
		\item Principal mecanismo de armazenamento:
		\item Os discos podem girar entre 60 e 250 vezes por segundo.
		\begin{description}
			\item[Taxa de transferência] Taxa de fluxo de dados entre o disco e o computador;
			\item[Tempo de posicionamento (random acess time)] Tempo para mover o disco entre o cilindro desejado (tempo de \alert{seek}) e o tempo para rotacionar a cabeça até encontrar o setor desejado (\alert{latência});
			\item[``Crash'' da cabeça] Acontece quando a cabeça do disco faz contato com a superfície.
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Outros barramentos}
	\begin{itemize}
		\item Discos conectados via barramento: EIDE, ATA, SATA, USB, Fibre Channel, SCSI, SAS, Firewire;
		\item Comunicação entre o barramento  e a \alert{controladora do disco};
		\item O \alert{tipo de barramento} afeta bastante a \alert{taxa de transferência}.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.5\textwidth]{../imagens/sas}
			\label{fig:sas}
			\caption{Barramento SAS x SATA \footnote{\url{http://www.serialstoragewire.org/Articles/2007_07/itinsights24.html}}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Disco rígido -- HD}
	\begin{itemize}
		\item Cilindros de metal com vários pratos;
		\item Unidades de medida importantes:
		\begin{itemize}
			\item Espaço de armazenamento;
			\item Taxa de rotação.
		\end{itemize}
		\item Performance \cite{galvin2013operating}:
		\begin{description}
			\item[Taxa de transferência] 6 GB/s (teórica);
			\item[Taxa efetiva] 1GB/s (real);
			\item[Tempo médio de seek] Cálculo baseado em $\frac{1}{3}$ das trilhas;
			\item[Latência média] $\frac{1}{2}$ da latência.
		\end{description}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Tempo de acesso -- HD}
	\begin{itemize}
		\item \alert{Latência de acesso} = \alert{Tempo médio de acesso} = tempo médio de \emph{seek} + latência média \cite{galvin2013operating}
		\begin{itemize}
			\item Discos rápidos: 3ms + 2ms = 5ms
			\item Discos lentos: 9ms + 5.56ms = 14.56ms
		\end{itemize}
		\item Tempo médio para I/O: Tempo médio de acesso (total de dados a transferir / taxa de transferência) + \emph{overhead} da controladora
	\end{itemize}
	\begin{table}[ht!]
		\begin{tabular}{|c|c|c|} \hline
			\hline
			\textbf{RPM} & \textbf{Tempo de seek} (ms) & \textbf{Latência média} (ms) \\ \hline
			\hline
			4.200 & 9,0 & 7,1 \\ \hline 
			5.400 & 8,0 & 5,6 \\ \hline 
			7.200 & 6,0 & 4,2 \\ \hline 
			10.000 & 4,5 & 3,0 \\ \hline 
			15.000 & 3,0 & 2,0 \\ \hline 
			\hline		
		\end{tabular}
		\caption{Latência dos discos rígidos \cite{latency}}
		\label{tab:latency}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Latência}
	\begin{itemize}
		\item A utilização de outros barramentos pode melhorar bastante a latência;
		\item Análise importante: custo x benefício;
		\item Discos maiores possuem um tempo maior de latência, pois o caminho que o cabeço tem que percorrer é maior.
	\end{itemize}
	\begin{table}[ht!]
		\begin{tabular}{|c|c|c|} \hline
			\hline
			\textbf{Barramento} & \textbf{Tempo de seek} (ms) & \textbf{Latência média} (ms) \\ \hline
			\hline
			2,5'' SAS 73GB & 3,3 & 2 \\ \hline 
			3,5'' SAS 146GB & 3,9 & 2 \\ \hline 
			3,5'' FC 146GB & 3,9 & 2 \\ \hline 
			\hline		
		\end{tabular}
		\caption{Latência em outros baramentos \footnote{\url{http://savepoint.blog.br/postgresql-discos-cia/}}}
		\label{tab:latency}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Discos do estado sólido -- SSD}
	\begin{itemize}
		\item Memória não volátil utilizada como disco rígido;
		\item Diversos mecanismos de armazenamento, mas em geral \alert{diminui de tamanho} com o tempo;
		\item O disco é tão rápido que o barramento pode ser um gargalo. Assim, podem ser conectados diretamente à interface PCI;
		\item Não possui latência rotacional ou tempo de \emph{seek};
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{SSD NAND}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{../imagens/ssd}
		\label{fig:ssd}
		\caption{Modelo SSD NAND \footnote{\url{https://www.extremetech.com/extreme/210492-extremetech-explains-how-do-ssds-work}}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Densidade SSD}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\textwidth]{../imagens/ssd-density}
		\label{fig:ssd-density}
		\caption{Densidade no SSD \footnote{\url{https://www.extremetech.com/extreme/210492-extremetech-explains-how-do-ssds-work}}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Tecnologia NVMe e comunicação}
	\begin{figure}
		\centering
		\includegraphics[width=0.4\textwidth]{../imagens/nvme01}
		\label{fig:nvme01}
		\caption{Tecnologia NVMe e barramento \footnote{\url{https://www.kingston.com/us/community/articledetail/articleid/48434}}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Velocidade de barramentos}
	\begin{figure}
		\centering
		\includegraphics[width=0.5\textwidth]{../imagens/nvme02}
		\label{fig:nvme02}
		\caption{Velocidade de SATA x PCI Express \footnote{\url{https://www.kingston.com/us/community/articledetail/articleid/48434}}}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Modelos de SSD}
	\begin{figure}
		\centering
		\includegraphics[width=0.5\textwidth]{../imagens/nvme03}
		\label{fig:nvme03}
		\caption{Modelos de SSD e comunicação \footnote{\url{https://www.kingston.com/us/community/articledetail/articleid/48434}}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Latência SSD}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{../imagens/ssd-latency}
		\label{fig:ssd-latency}
		\caption{Latência SSD \footnote{\url{https://www.extremetech.com/extreme/210492-extremetech-explains-how-do-ssds-work}}}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{SSD -- Tempo}
	\begin{center}
		\begin{figure}
			\includegraphics[width=1\textwidth]{../imagens/intel-x25e-cacheoff-4kb-1800s}
			\label{fig:intel-x25e-cacheoff-4kb-1800s}
			\caption{Tempo de escrita SSD \cite{benchmark}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Fitas magnéticas}
	\begin{itemize}
		\item Primeiro formato de armazenamento;
		\item Armazena grandes quantidades de informação;
		\item Tempo de acesso ~1000 vezes mais lento que o disco \cite{galvin2013operating};
		\item Tipicamente utilizadas para backup.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.6\textwidth]{../imagens/fita}
			\label{fig:fita}
			\caption{Fitas para armazenamento \footnote{Fonte: \url{http://www.jazz4you.net/wp-content/uploads/2015/10/LTO-6.jpg}}}
		\end{figure}
	\end{center}
\end{frame}

\section{Estrutura dos discos}

\begin{frame}
	\frametitle{Organização da estrutura}
	\begin{itemize}
		\item Os discos normalmente são endereçados como \alert{arrays unidimensionais} que utilizam \alert{blocos lógicos};
		\item Os blocos são mapeados no disco sequencialmente;
		\item Organização em \alert{setores} e \alert{trilhas};
		\item\alert{Tracks} e \alert{bad sectors};
		\item É possível ainda \alert{adicionar discos};
		\item Comunicação por diferentes barramentos: SCISI, FC, etc.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Conjuntos de discos}
	\begin{itemize}
		\item É possível conectar os discos uns nos outros criando \alert{array} de discos;
		\item Mecanismos para criação de \alert{array} de discos \cite{galvin2013operating}:
		\begin{itemize}
			\item Portas de comunicação exclusiva;
			\item Memória, algum software de controle;
			\item RAID, \emph{hot spare}, \emph{hot swap};
			\item Armazenamento compartilhado;
			\item Implementações específicas para o compartilhamento de recursos (replicação, cluster, etc.)
		\end{itemize}
		\item Utilização de um ou mais mecanismos, além de combinações entre eles.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Storage Area Network -- SAN}
	\begin{itemize}
		\item \alert{Definição}: um ou mais array de discos conectado(s);
		\item Facilidade de adicionar e remover dispositivos de armazenamento.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/san}
			\label{fig:san}
			\caption{Exemplo de SAN \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Network Attached Storage -- NAS}
	\begin{itemize}
		\item \alert{Definição}: um mecanismo de armazenamento conectado pela rede ao invés de um barramento local;
		\item Alguns protocolos: NFS, CIFS e iSCSI.
	\end{itemize}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.8\textwidth]{../imagens/nas}
			\label{fig:nas}
			\caption{Exemplo de NAS \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\section{Organização dos discos}

\begin{frame}
	\frametitle{Formatação}
	\begin{itemize}
		\item \alert{Definição}: dividir o disco em pedaços pequenos para leitura e escrita por parte da controladora do disco;
		\item Antes de armazenar os dados no disco, o sistema operacional precisa gravar seus próprios registros;
		\begin{description}
			\item[Bloco] Menor unidade de armazenamento;
			\item[Particionar] Dividir o disco em grupos de blocos, organizados dentro dos cilindros;
			\item[Sistema de arquivos] Algoritmo de organização dos blocos;
			\item[Formatação lógica] Construir o sistema de arquivos.
		\end{description}
		\item Tratamento especial das partições do sistema operacional;
		\item O tamanho do bloco \alert{faz diferença}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Utilização de espaço}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.9\textwidth]{../imagens/espaco-transferencia}
			\label{fig:espaco-transferencia}
			\caption{Utilização de espaço e taxa de transferência \cite{hirata}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Boot Windows}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.6\textwidth]{../imagens/boot-windows}
			\label{fig:boot-windows}
			\caption{Mecanismo de boot do Windows \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Acessando os dados}
	\begin{itemize}
		\item Também é parte importante pensar onde será carregado o sistema operacional;
		\item Partições \alert{especiais};
		\item O arquivo é identificado por uma série de atributos;
		\item Endereço lógico mapeado para endereço físico;
		\item Diferentes algoritmos de armazenamento/recuperação de endereços lógicos: sistemas de arquivos;
		\item Conceito de \alert{descritor de arquivo}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Acesso ao arquivo}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.85\textwidth]{../imagens/inodes}
			\label{fig:inodes}
			\caption{Acesso ao arquivo utilizando inodes \cite{hirata}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{RAID}
	\begin{itemize}
		\item \emph{Redundant Array of Inexpensive Disks} -- RAID;
		\item Aumenta o \alert{tempo médio de falha};
		\item Diminui bastante o \alert{tempo médio de recuperação};
		\item O \alert{tempo médio de perda} tem como base os dois fatores acima;
		\item objetivo principal: aumentar a \alert{capacidade de armazenamento} e a \alert{segurança};
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{RAID -- Implementações}
	\begin{description}
		\item[RAID 1] \alert{Mirroring} ou \alert{shadowing}: duplica o conteúdo nos vários discos;
		\item[RAID 1 + 0] \alert{Stripped mirrors} ou \alert{mirrored stripes} (0+1) maior performance e confiabilidade;
		\item[RAID 4, 5 e 6] Paridade de bloco. Utiliza muito menos redundância.
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{RAID -- Níveis}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.3\textwidth]{../imagens/raid}
			\label{fig:raid}
			\caption{Níveis de RAID \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{RAID -- Striping}
	\begin{center}
		\begin{figure}
			\includegraphics[width=0.45\textwidth]{../imagens/raid-striping}
			\label{fig:raid-string}
			\caption{Striping no RAID \cite{galvin2013operating}}
		\end{figure}
	\end{center}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../so}
\bibliographystyle{apalike}

\end{document}
