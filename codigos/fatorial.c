float fat(int n) {
    int x; /* define uma variável local x */
    x=n-1;

    if (n == 0) return (1);
    else return (n * fat(x)); /* ao invés de passar n-1, passa-se x */
}

int main() {
    int i=0;

    while (i>=0){
        printf("Entre valor inteiro >=0 ou valor <0 para terminar:");
        scanf("%d", &i);
        if (i>=0) printf("Fatorial de %d = %f\n", i, fat(i));
    }

    return 0;
}
