// loop principal para evolucao de geracoes
#pragma omp parallel \
    shared(grid,newGrid,maxIter,dim) \
    private(iter,i,j)
{ // principal regiao paralela

    for (iter = 0; iter<maxIter; iter++) {
        // Colunas esquerda e direita
        #pragma omp for
        for (i = 1; i<=dim; i++) {
            // copiar ultima coluna real para coluna
            // esquerda de borda (left ghost column)
            grid[i][0] = grid[i][dim];
            // copiar primeira coluna real para coluna
            // direita de borda (right ghost column)
            grid[i][dim+1] = grid[i][1];
        }
        // Linhas superior e inferior
        #pragma omp for
        for (j = 0; j<=dim+1; j++) {
            grid[0][j] = grid[dim][j];
            grid[dim+1][j] = grid[1][j];
        }
        // Loop sobre as celulas para nova geracao
        #pragma omp for
        for (i = 1; i<=dim; i++) {
            for (j = 1; j<=dim; j++) {
                // calcula numero de vizinhos
                int numNeighbors = getNeighbors(grid, i, j);
                // aplicacao das regras do GOL
                if (grid[i][j]==1 && numNeighbors<2)
                newGrid[i][j] = 0;
                else if (grid[i][j]==1 &&
                (numNeighbors==2 || numNeighbors==3))
                newGrid[i][j] = 1;
                else if (grid[i][j]==1 && numNeighbors>3)
                newGrid[i][j] = 0;
                else if (grid[i][j]==0 && numNeighbors == 3)
                newGrid[i][j] = 1;
                else
                newGrid[i][j] = grid[i][j];
        }
}
// troca de arrays para proxima geracao
#pragma omp single
{ // regiao atomica para troca de ponteiros
int **tmpGrid = grid;
grid = newGrid;
newGrid = tmpGrid;
} // fim da regiao atomica
} // Fim do laco principal
Fim da regiao paralela principal
