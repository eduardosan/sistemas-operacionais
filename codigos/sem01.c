#define N 100
typedef int semaphore
semaphore mutex = 1; /* controla região crítica */
semaphore empty = N; /* controla posições vazias */
semaphore full = 0; /* controla posições ocupadas */
