void producer(void ) {
	while (TRUE) {
		item = produce_item ();
		down (&empty);
		down (& mutex );
		insert_item(item ); /* R_critica */
		up(&mutex );
		up(&full );
	}
}
