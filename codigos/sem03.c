void consumer(void ) {
	while (TRUE) {
		down (&full);
		down (& mutex );
		item = remove_item (); /* R_cr í tica */
		up(&mutex );
		up(&empty );
		consume_item(item);
	}
}
