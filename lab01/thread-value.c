#define _GNU_SOURCE
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sched.h>
#include <stdio.h>

// 64kB stack
#define FIBER_STACK 1024*64

int value = 5;

// The child thread will execute this function
int threadFunction( void* argument )
{
    printf("child thread exiting\n");
	value += 15;
	printf ("CHILD: value = %d\n",value); /* LINE A */
    return 0;
}

int main()
{
    void* stack;
    pid_t pid;

    // Allocate the stack
    stack = malloc( FIBER_STACK );
    if ( stack == 0 )
    {
    	perror("malloc: could not allocate stack");
    	exit(1);
    }

    printf( "Creating child thread\n" );
	printf ("PARENT: value = %d\n", value); /* LINE A */

    // Call the clone system call to create the child thread
    pid = clone( &threadFunction, (char*) stack + FIBER_STACK,
    	SIGCHLD | CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM, 0 );
    if ( pid == -1 )
    {
        perror( "clone" );
        exit(2);
    }

    // Wait for the child thread to exit
    pid = waitpid( pid, 0, 0 );
	printf ("PARENT: value = %d depois do wait\n", value); /* LINE A */
    if ( pid == -1 )
    {
		perror( "waitpid" );
		exit(3);
    }

    // Free the stack
    free( stack );
    printf("Child thread returned and stack freed.\n");

    return 0;
}
